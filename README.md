## Docker Webcontainer - nginx+mysql8+php8.1

---

### Description
This Composer package comes with a basic Docker web container setup. The following containers are installed.
1. Nginx
2. MySQL 8
3. PHP 8.1 [FPM, XDebug]

---

### Installation
1. Integrate the Composer Package into your website
```shell
composer require twoh/dockersetup
```
2. Copy into project and adapt to your needs
```shell
cp ./vendor/twoh/dockersetup/docker-compose.yaml .
```
4. Copy into your project and adapt to your needs
```shell
cp ./vendor/twoh/dockersetup/.env~dist .env
```

---

### Notes
To get all future updates for php8.1 setup, you can include the docker setup manually into your composer installation.
```json
"repositories": {
    "dockerSetupPhp8Webcontainer": {
        "type": "vcs",
        "url": "git@gitlab.com:twoh-typo3-ext/boilerplate/dockersetup.git"
    },
},
"require": {
    "twoh/dockersetup": "8.1.*"
},
```